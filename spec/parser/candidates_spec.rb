# frozen_string_literal: true

require 'speculations/parser'
RSpec.describe Speculations::Parser do

  let(:parser) { described_class.new }
  let!(:ast)   { parser.parse_from_file(fixture) }
  let(:fixture){ fixtures_path("CANDIDATES.md") }

  describe "has one child context" do
    it { expect( ast.children ).to be_one }
  end

  it "has no examples" do
    expect( ast.examples ).to be_empty
  end

  describe "has one include" do
    let(:incl) { ast.children.first }
    it { expect( incl.includes ).to be_one }
    it "has correct text" do
      expect( incl.includes.first.to_code ).to eq(["    # spec/fixtures/CANDIDATES.md:8", "    blah"])
    end
  end

  describe "debugging" do
    let :expected_output do 
      <<~EOS
       4: maybe_include match in :includes
       2: includes-> candidate
       5: context match in :candidate
       3: candidate-> out
       7: maybe_include match in :out
       5: out-> candidate
       8: maybe_include match in :candidate
       9: ruby_code_block match in :candidate
       7: candidate-> in
       11: eblock match in :in
       9: in-> includes
       EOS
    end

    it "debugs to standard error" do
      expect do
        parser.parse_from_file(fixture, debug: true)
      end.to output(expected_output).to_stderr_from_any_process
    end
  end

end
#  SPDX-License-Identifier: AGPL-3.0-or-later
