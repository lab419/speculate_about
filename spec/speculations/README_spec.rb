# DO NOT EDIT!!!
# This file was generated from "README.md" with the speculate_about gem, if you modify this file
# one of two bad things will happen
# - your documentation specs are not correct
# - your modifications will be overwritten by the speculate command line
# YOU HAVE BEEN WARNED
RSpec.describe "README.md" do
  # README.md:71
  context "Specing the Great Question" do
    # README.md:75
    let(:answer) { 42 }
    # README.md:81
    def question?; answer end
    it "the correct answer is: (README.md:87)" do
      expect(answer).to eq(question?)
    end
  end
end