require_relative "lib/speculations/version.rb"
Gem::Specification.new do |s|
  s.name        = 'speculate_about'
  s.version     = Speculations::VERSION
  s.date        = Time.new.strftime('%Y-%m-%d')
  s.summary     = 'Extract RSpecs from Markdown'
  s.description = 'Allows Markdown or other text files to be used as literal specs, à la Elixir doctest, but from any file.'
  s.authors     = ['Robert Dober']
  s.email       = 'robert.dober@gmail.com'
  s.files       = Dir.glob('{lib,bin}/**/*.rb')
  s.bindir      = 'bin'
  s.executables << 'speculate'
  s.homepage    = 'https://codeberg.org/lab419/speculate_about'
  s.license     = 'AGPL-3.0-or-later'

  s.required_ruby_version = '>= 3.3.0'
end

# SPDX-License-Identifier: AGPL-3.0-or-later
