# frozen_string_literal: true

module Kernel
  def debug__(subject=nil, msg: nil, rtrn: nil, debug:)
    return rtrn || subject unless debug

    $stderr.puts(msg) if msg
    $stderr.puts(subject.inspect) if subject
    rtrn || subject
  end

  def dbg_match(msg, lnb, debug:)
    designation = name.split("::").last.downcase
    debug__(msg: "#{lnb.succ}: #{msg} match in :#{designation}", debug:)
  end
end
# SPDX-License-Identifier: Apache-2.0
