# frozen_string_literal: true

module Speculations
  VERSION = "1.0.5"
end
# SPDX-License-Identifier: AGPL-3.0-or-later
