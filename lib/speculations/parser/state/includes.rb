require_relative 'context_maker'
module Speculations
  class Parser
    module State
      module Includes extend self
        include ContextMaker

        def parse line, lnb, node, _ctxt, debug: true
          case
          when match = State.context_match(line)
            dbg_match("context", lnb, debug:)
            make_new_context(lnb: lnb, node: node, match: match)
          when match = State.maybe_include(line)
            dbg_match("maybe_include", lnb, debug:)
            [:candidate, node, :inc]
          when match = State.maybe_example(line)
            dbg_match("maybe_example", lnb, debug:)
            [:candidate, node, match[:title]]
          else
            [:includes, node]
          end
        end

      end
    end
  end
end
