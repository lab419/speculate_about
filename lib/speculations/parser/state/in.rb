# frozen_string_literal: true

module Speculations
  class Parser
    module State
      module In extend self
        include State
        def parse line, lnb, node, ctxt, debug: true
          case
          when State.eoblock_match(line)
            dbg_match("eblock", lnb, debug:)
            [ctxt, node.parent]
          else
            [:in, node.add_line(line), ctxt]
          end
        end
      end
    end
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
