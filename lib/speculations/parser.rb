# frozen_string_literal: true

require_relative '../debugging'
require_relative './parser/context'
require_relative './parser/state'
module Speculations
  class Parser

    def self.parsers
      @__parsers__ ||= {
        candidate: State::Candidate,
        in: State::In,
        includes: State::Includes,
        out: State::Out
      }
    end

    def parse_from_file(file, debug: false)
      @filename = file
      @input = File
        .new(file)
        .each_line(chomp: true)
        .lazy
      parse!(debug:)
    end

    private

    def initialize
      @state = :out
    end

    def parse!(debug:)
      root = node = Context.new(filename: @filename)
      ctxt = nil
      @input.each_with_index do |line, lnb|
        parser = self.class.parsers.fetch(@state)
        old_state = @state
        @state, node, ctxt = parser.parse(line, lnb.succ, node, ctxt, debug:)
        debug__(msg: "#{lnb}: #{old_state}-> #{@state}", debug:) if @state != old_state
      end
      root
    end
  end
end
# AGPL-3.0-or-later
